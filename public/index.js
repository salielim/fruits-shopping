(function () {
    var FruitsApp = angular.module("FruitsApp", []);

    var FruitsCtrl = function () {
        var fruitsCtrl = this;

        fruitsCtrl.inventory = [
            ["acorn_squash.png", "Acorn Squash", 1], ["apple.png", "Apple", .5],
            ["bell_pepper.png", "Bell Pepper", .3], ["blueberries.png", "Blueberries", .2],
            ["broccoli.png", "Broccoli", 2], ["carrot.png", "Carrot", 1],
            ["celery.png", "Celery", 1], ["chili_pepper.png", "Chili Pepper", .5],
            ["corn.png", "Corn", 1], ["eggplant.png", "Eggplant", .75],
            ["lettuce.png", "Lettuce", 1], ["mushroom.png", "Mushroom", 2],
            ["onion.png", "Onion", 2], ["potato.png", "Potato", 1],
            ["pumpkin.png", "Pumpkin", 1], ["radish.png", "Radish", .5],
            ["squash.png", "Squash", 2], ["strawberry.png", "Strawberry", 1.5],
            ["sugar_snap.png", "Sugar Snap", 1], ["tomato.png", "Tomato", 1],
            ["zucchini.png", "Zucchini", 1]
        ];

        fruitsCtrl.fruit = [];
        fruitsCtrl.qty = "";
        fruitsCtrl.fruitList = [];

        fruitsCtrl.addToList = function () {
            fruitsCtrl.fruit = JSON.parse(fruitsCtrl.fruit);
            fruitsCtrl.fruit.push(fruitsCtrl.qty);
            fruitsCtrl.fruitList.push(fruitsCtrl.fruit);
            // fruitsCtrl.fruitList = JSON.parse(fruitsCtrl.fruitList);
            console.log("Add: " + fruitsCtrl.fruit);
            console.log("New Arr: " + fruitsCtrl.fruitList);
            fruitsCtrl.fruit = [];
            fruitsCtrl.qty = "";
        }

        fruitsCtrl.delete = function ($index) {
            fruitsCtrl.fruitList.splice($index, 1);
        }
    };

    FruitsApp.controller("FruitsCtrl", FruitsCtrl);
})();