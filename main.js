var express = require("express");
var app = express();

app.use(
    express.static(__dirname + "/public")
);

app.use(
    express.static(__dirname + "/bower_components")
);

app.use("/index.html", function(req, res) {
    res.status(200);
    res.type("text/html");
});

if (process.env.PROD) {
    console.log("I am running in production");
} else {
    console.log("This is testing");
}
// try in command line
    // export PROD="1"
    // node main.js
    // or PROD=1 node main.js

var port = parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000;
console.log("port = %d", port);
// try in command line
    // export APP_PORT="5555"
    // node main.js

app.listen(
    port,
    function() {
        console.log("application started on port %d", port);
    }
);